os.loadAPI("os/registry/libs/aes")
os.loadAPI("os/registry/libs/generic")

local secondsLeft = 2

term.clear()
term.setCursorPos(1, 1)

term.setTextColor(colors.red)

local header = [[
             /\_/\____,
   ,___/\_/\ \  ~     /
   \     ~  \ )   XXX
    XXX     /    /\_/\___,
       \o-o/-o-o/   ~    /
        ) /     \    XXX
       _|    / \ \_/
    ,-/   _  \_/   \
   / (   /____,__|  )
  (  |_ (    )  \) _|
 _/ _)   \   \__/   (_
(,-(,(,(,/      \,),),)

Cerberus Targeting System

]]


local OS_PROTOCOL = generic.getProtocol()
local launch_key = ""

local keyTurns = 0
local shouldListen = true

print(header)
if not fs.exists("os/registry/key.txt") then
    print("[CERBERUS] > No launch authorization detected, running minimal security!")
else
    local k = fs.open("os/registry/key.txt", "r")
    local ke = fs.readAll("os/registry/key.txt")
    if ke ~= "" then
        launch_key = aes.decrypt("america", ke)
    else
        print("[CERBERUS] > No launch authorization detected, running minimal security!")
    end
end
print("[CERBERUS] > Awaiting Orders...")

function triggerLaunch()
    print("[CERBERUS] > Launch initiated, standby...")
    print("[CERBERUS] > SEALING BLAST DOORS!")
    redstone.setOutput("right", true)
    os.sleep(2)
    print("[CERBERUS] > OPENING SILO DOOR!")
    redstone.setOutput("left", true)
    os.sleep(5)
    print("[CERBERUS] > 5...")
    os.sleep(1)
    print("[CERBERUS] > 4...")
    os.sleep(1)
    print("[CERBERUS] > 3...")
    os.sleep(1)
    print("[CERBERUS] > 2...")
    os.sleep(1)
    print("[CERBERUS] > 1...")
    os.sleep(1)
    print("[CERBERUS] > LAUNCHING MISSILE!")
    redstone.setOutput("back", true)
    os.sleep(2)
    term.clear()
    term.setCursorPos(1, 1)
    term.setTextColor(colors.red)
    print(header)
    print("⚠ SYSTEM REBOOT REQUIRED ⚠")
end

while true do
    if keyTurns > 0 then
        os.sleep(2)
        if keyTurns <= 2 then
            keyTurns = 0
            print("[CERBERUS] > Reset keys turns to 0")
        else
            shouldListen = false
            triggerLaunch()
            break
        end
    end
end

while shouldListen do
    sID,msg,proto = rednet.receive(OS_PROTOCOL)
    if string.find(msg, "|") then
        local m_s = generic.strSplit(msg, "|")
        if m_s[1] == "verify" then
            local check = aes.decrypt("america", m_s[2])
            if check == launch_key then
                keyTurns = keyTurns + 1
                print("[CERBERUS] > Valid authorization from: " .. sID)
            else
                print("[CERBERUS] > Failed authorization from: " .. sID)
            end
        end
    end
end