function getServerOSVersion()
    local wh = http.get("https://bitbucket.org/Dolware/cerberus/raw/e398e3eef0250405b87973f2ef155a0089be5db0/server_version.txt")
    local ver = "UNKNOWN_VERSION"
    if wh then
        ver = wh.readAll()
    else
        ver = "UNKNOWN_VERSION"
    end

    return ver
end