function strStartsWith(input, ch)
    return input:sub(1, #ch) == ch
end

function strSplit(inputstr, sep)
    local Table = {}
    local fpat = "(.-)" .. sep
    local last_end = 1
    local s, e, cap = inputstr:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            table.insert(Table, cap)
        end
        last_end = e + 1
        s, e, cap = inputstr:find(fpat, last_end)
    end
    if last_end <= #inputstr then
        cap = inputstr:sub(last_end)
        table.insert(Table, cap)
    end
    return Table
end

function getModem()
    local perList = peripheral.getNames()
    local m = nil
    if #perList > 0 then
        for i = 1, #perList do
            if peripheral.getType(perList[i]) == "modem" then
                m = perList[i]
            end
        end
    end
    return m
end

function getUsername()
    local regFiles = fs.list("os/registry")
    local uname = nil
    for fi=1,#regFiles do
        if generic.strStartsWith(regFiles[fi], "u_") then
            local userString = generic.strSplit(regFiles[fi], "_")
            uname = userString[2]
        end
    end
    return uname
end

function getProtocol()
    local regFiles = fs.list("os/registry")
    local uname = nil
    for fi=1,#regFiles do
        if generic.strStartsWith(regFiles[fi], "pr_") then
            local userString = generic.strSplit(regFiles[fi], "_")
            uname = userString[2]
        end
    end
    return uname
end

function displayPeripherals()
    local perList = peripheral.getNames()
    if #perList > 0 then
        for i = 1, #perList do
            print("[EDIT/libs/generic]: " .. perList[i] .. " | " .. peripheral.getType(perList[i]))
        end
    end
end