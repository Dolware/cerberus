os.loadAPI("os/registry/libs/generic")
os.loadAPI("os/registry/libs/base64")

local username = generic.getUsername()
local password = ""
local protocol = ""
local regFiles = fs.list("os/registry")
for fi=1,#regFiles do
    if generic.strStartsWith(regFiles[fi], "p_") then
        local passString = generic.strSplit(regFiles[fi], "_")
        password = base64.decode(passString[2])
    elseif generic.strStartsWith(regFiles[fi], "pr_") then
        local passString = generic.strSplit(regFiles[fi], "_")
        password = passString[2]
    end
end

shell.run("rm", "os")
shell.run("rm", "startup")
shell.run("rm", "*")
shell.run("wget", "http://dolware.cf/CC/CERBERUS.tar", "CERBERUS.tar")
shell.run("tar", "-xvf", "CERBERUS.tar")
shell.run("cd", "../")
shell.run("rm", "disk/CERBERUS.tar")
shell.run("rm", "tar")
shell.run("rm", "update")
shell.run("install", username, password, protocol)