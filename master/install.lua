os.loadAPI("libs/base64.lua")

local args = { ... }

term.clear()

if #args ~= 3 then
    print("Invalid arguments for OS registry(UNAME, PASS, PROTOCOL)")
    return
else
    print("Installing OS...")

    local RegistryStructure = {"os/registry", "os/registry/libs", "os/registry/p_", "os/registry/pr_", "os/registry/u_"}
    
    for i=1,#RegistryStructure do
        if not fs.exists(RegistryStructure[i]) then
            fs.makeDir(RegistryStructure[i])
        end
    end

    fs.copy("libs/generic.lua", "os/registry/libs/generic")
    fs.copy("libs/base64.lua", "os/registry/libs/base64")
    --fs.copy("libs/json.lua", "os/registry/libs/json")
    fs.copy("libs/networking.lua", "os/registry/libs/networking")
    fs.copy("libs/aes.lua", "os/registry/libs/aes")

    fs.move("os/registry/p_", "os/registry/p_" .. base64.encode(args[2]))
    fs.move("os/registry/pr_", "os/registry/pr_" .. args[3])
    fs.move("os/registry/u_", "os/registry/u_" .. args[1])

    sleep(1)

    fs.copy("libs/tar.lua", "tar")
    fs.copy("update.lua", "update")
    fs.copy("cerberus-bl.lua", "startup")
    fs.copy("listen.lua", "listen")

    if fs.exists("startup") then
        print("OS installed, rebooting...")
        sleep(1)
        os.reboot()
    else
        print("Failed to install OS, check root dir!")
    end
    os.unloadAPI("libs/base64.lua")
end